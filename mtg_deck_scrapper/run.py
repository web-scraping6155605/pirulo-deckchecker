from decklister import MTGArenaParser, CardsFinder


if __name__ == "__main__":
    decklist = """Commander
1 Nahiri, Forged in Fury

Deck
1 Thousand Moons Smithy
1 Vulshok Splitter
1 Ancestral Blade
1 Barbed Spike
1 Goldwarden's Helm
1 Hexgold Halberd
1 Barbed Batterfist
1 Hexgold Hoverwings
1 Bladehold War-Whip
1 Skinwing
1 Sickleslicer
1 Flayer Husk
1 Batterbone
1 Citizen's Crowbar
1 Dragonwing Glider
1 Diamond Pick-Axe
1 Loyal Warhound
1 Puresteel Paladin
1 Beamtown Beatstick
1 Goldvein Pick
1 Kemba's Banner
1 Deconstruction Hammer
1 Glimmer Lens
1 Prying Blade
1 Nettlecyst
1 Bloodforged Battle-Axe
1 Hexplate Wallbreaker
1 Fervent Champion
1 Knight of the White Orchid
1 Reyav, Master Smith
1 Sram, Senior Edificer
1 Akiri, Fearless Voyager
1 Lion Sash
1 Bruenor Battlehammer
1 Hammer of Nazahn
1 Brilliant Restoration
1 Goldwardens' Gambit
1 Mask of Memory
1 Sunforger
1 Argentum Armor
1 Austere Command
1 Astor, Bearer of Blades
1 Danitha Capashen, Paragon
1 Dowsing Device
1 Curse of Opulence
1 Akki Battle Squad
1 Stone Haven Outfitter
1 Rabbit Battery
1 Commander Liara Portyr
1 Command Tower
1 Temple of Triumph
1 Furycalm Snarl
1 Sacred Peaks
1 Alpine Meadow
1 Battlefield Forge
1 Spire of Industry
14 Plains
13 Mountain
1 Clifftop Retreat
1 Kabira Takedown
1 Boros Garrison
1 Boros Charm
1 Jor Kadeen, First Goldwarden
1 Neyali, Suns' Vanguard
1 Bladegraft Aspirant
1 Lizard Blades
1 Frodo, Determined Hero
1 Nahiri's Resolve
1 Response // Resurgence
1 Generous Gift
1 Forge Anew
1 Sol Ring
1 Maul of the Skyclaves
1 Kaldra Compleat
"""
    cards = MTGArenaParser().parse_decklist(decklist)
    #cards = ['Batterbone', 'Command Tower']
    finder = CardsFinder(cards)
    finder.find_decklist(cards)
    finder.dump_cards('test.json')

    total_price = 0
    found_cards = []
    for card in finder.found_cards:
        values = list(card.values())
        total_price += float(values[0][0]['price'])
        card_name = list(card.keys())[0]
        found_cards.append(card_name)
    print(total_price)
    print(set(cards) - set(found_cards))
