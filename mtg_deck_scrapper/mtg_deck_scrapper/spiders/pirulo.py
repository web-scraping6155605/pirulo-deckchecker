from typing import Iterable

import requests
import scrapy
from scrapy import Request
from urllib.parse import quote_plus
from scrapy.utils.reactor import install_reactor


class PiruloSpider(scrapy.Spider):
    name = "pirulo"
    allowed_domains = ["www.mtgpirulo.com"]
    start_urls = ["https://www.mtgpirulo.com"]
    install_reactor("twisted.internet.asyncioreactor.AsyncioSelectorReactor")

    def start_requests(self) -> Iterable[Request]:
        card = self.cards_list[0]
        card_name = quote_plus(card)
        meta = {'items': []}

        self.session = requests.Session()

        yield scrapy.Request(
            f"https://www.mtgpirulo.com/advanced_search?utf8=%E2%9C%93&search%5Bfuzzy_search%5D={card_name}&search%5Btags_name_eq%5D=&search%5Bsell_price_gte%5D=&search%5Bsell_price_lte%5D=&search%5Bbuy_price_gte%5D=&search%5Bbuy_price_lte%5D=&search%5Bin_stock%5D=0&search%5Bin_stock%5D=1&search%5Bcategory_ids_with_descendants%5D%5B%5D=&search%5Bcategory_ids_with_descendants%5D%5B%5D=&search%5Bsort%5D=name&search%5Bdirection%5D=ascend&commit=Buscar&search%5Bcatalog_group_id_eq%5D=",
            meta=meta)

    def _add_to_cart(self, card_id, csrf_token):
        payload = {"line_items": [{"variant_id": card_id, "qty": 1}]}

        try:
            self.session.post('https://www.mtgpirulo.com/api/v1/cart/line_items', json=payload,
                                headers={'X-CSRF-Token': csrf_token}, timeout=0.0000000001)
        except requests.exceptions.ConnectTimeout:
            pass


    def _is_better(self, card, current_price, lower=True):
        return card['price'] < current_price or (card['price'] > current_price and not lower)

    def _filter_price(self, cards) -> str:
        current_price = None
        best_choice = None
        for card in cards:
            cart_id = card.pop('cart_id')
            if not current_price:
                best_choice = cart_id
                current_price = card['price']
            elif self._is_better(card, current_price):
                best_choice = cart_id
                current_price = card['price']
        return best_choice

    def _parse_search(self, cards_crawled):
        target_card = self.cards_list[0]
        matches = []
        for card_crawled in cards_crawled:
            card_name_unprocessed = card_crawled.css('.meta a h4::text').get()
            card_url = 'https://www.mtgpirulo.com' + card_crawled.css('.meta a::attr(href)').get()
            card_price = card_crawled.css(".regular.price::text").get()[1:]
            crawled_card_name = card_name_unprocessed.split(" -")[0]

            if crawled_card_name.lower() == target_card.lower():
                try:
                    found = {
                        'card_name': card_name_unprocessed,
                        'url': card_url,
                        'price': float(card_price),
                        'cart_id': card_crawled.css(".add-to-cart-form::attr(data-vid)").get()
                    }
                    matches.append(found)
                except ValueError:
                    print(
                        f"""Error in card: {card_name_unprocessed}, the webpage probably showed a card that was not 
                        in stock as being available, please contact the developer if you suspect that's not the case""")
        return matches

    def parse(self, response, **kwargs):
        cards_crawled = response.xpath('/html/body/div[1]/div/section/div/div[3]/section/div/div[5]/ul//li')
        target_card = self.cards_list[0]
        item = {target_card: self._parse_search(cards_crawled)}

        csrf_token = response.css('meta[name="csrf-token"]::attr(content)').get()
        cookie = str(response.headers.get("Set-Cookie")).split('=')[1].split(";")[0]
        self.session.cookies.set("_secure_frontend_session_id", cookie, domain='www.mtgpirulo.com')

        if item[target_card]:
            best_choice_id = self._filter_price(item[target_card])
            self._add_to_cart(best_choice_id, csrf_token)
            response.meta['items'].append(item)

        self.cards_list = self.cards_list[1:]

        if len(self.cards_list) == 0:
            print(response.meta)
            for item in response.meta['items']:
                yield item
            print("// COOKIES FOR CART WITH ITEMS ADDED //")
            print(self.session.cookies)
        else:
            next_card_name = self.cards_list[0]
            yield scrapy.Request(
                f"https://www.mtgpirulo.com/advanced_search?utf8=%E2%9C%93&search%5Bfuzzy_search%5D={next_card_name}&search%5Btags_name_eq%5D=&search%5Bsell_price_gte%5D=&search%5Bsell_price_lte%5D=&search%5Bbuy_price_gte%5D=&search%5Bbuy_price_lte%5D=&search%5Bin_stock%5D=0&search%5Bin_stock%5D=1&search%5Bcategory_ids_with_descendants%5D%5B%5D=&search%5Bcategory_ids_with_descendants%5D%5B%5D=&search%5Bsort%5D=name&search%5Bdirection%5D=ascend&commit=Buscar&search%5Bcatalog_group_id_eq%5D=",
                meta=response.meta, callback=self.parse, cookies={"_secure_frontend_session_id": cookie},
                headers={'X-CSRF-Token': csrf_token})
