import json
from abc import ABC, abstractmethod
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
from scrapy.signalmanager import dispatcher
from scrapy import signals
from twisted.internet import reactor
import jmespath as jp


class Dumper:
    @abstractmethod
    def dump_cards(self, output_file, cards_json):
        ...


class DecklistDumper(Dumper):
    @staticmethod
    def _craft_dump(cards_json):
        pattern = jp.compile("[].*[][].card_name")
        decklist_str = ""
        for card_found in pattern.search(cards_json).split(" -")[0]:
            decklist_str += card_found + "\n"
        return decklist_str

    def dump_cards(self, output_file, cards_json):
        dump = self._craft_dump(cards_json)
        with open(output_file, "w") as output_path:
            json.dump(dump, output_path)


class JsonDumper(Dumper):
    def dump_cards(self, output_file, cards_json):
        with open(output_file, 'w') as output_path:
            json.dump(cards_json, output_path)


class DumperFactory:
    def __init__(self):
        self.mappings = {'txt': DecklistDumper, 'json': JsonDumper}

    def create(self, hint):
        return self.mappings[hint]()

dumper_factory = DumperFactory()


class CardsFinder:
    def __init__(self, decklist: list, webpage):
        self.decklist = decklist
        self.found_cards = []
        self.webpage = webpage

        dispatcher.connect(self._append_result, signal=signals.item_passed)

    def _reset_process(self):
        self.process = CrawlerProcess(get_project_settings())

    def _append_result(self, signal, sender, item, response, spider):
        self.found_cards.append(item)

    def _crawl_page(self, cards):
        self.process.crawl(self.webpage, cards_list=cards)

    def find_card(self, card):
        self._reset_process()
        self._crawl_page([card])
        self.process.start()

    def find_decklist(self):
        self._reset_process()
        self._crawl_page(self.decklist)
        self.process.start()

    def dump_cards(self, out_path):
        with open(out_path, 'w') as file:
            json.dump(self.found_cards, file, indent=2)

    def get_missing_cards(self):
        pattern = jp.compile("[].*[][].card_name")
        card_names = pattern.search(self.found_cards).split(" -")[0]
        return set(self.decklist) - set(card_names)

    def found_price(self):
        ...


class MTGArenaParser:
    @staticmethod
    def parse_decklist(decklist):
        raw_lines = [line for line in decklist.splitlines() if len(line.split(" ")) > 1]
        card_names = []
        for line in raw_lines:
            card_name_index = line.index(" ") + 1
            card_names.append(line[card_name_index:])
        cards = [card_name for card_name in card_names]
        return cards
