import argparse
import json

from decklister import MTGArenaParser, CardsFinder

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('-w', '--webpage', type=str, help='an integer for the accumulator')
parser.add_argument('-i', '--input_file', type=str, help='an integer for the accumulator')
parser.add_argument('-o', '--output_file', type=str, help='an integer for the accumulator')

args = parser.parse_args()

with open(args.input_file) as input_file:
    decklist = input_file.read()

cards = MTGArenaParser().parse_decklist(decklist)

finder = CardsFinder(cards, args.webpage)
finder.find_decklist()

finder.dump_cards(args.output_file)
#
#print("CARDS NOT FOUND:")
#for missing in finder.get_missing_cards():
#    print(missing)
