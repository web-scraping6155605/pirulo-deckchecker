# MTG deck buying scraper (WIP)

This is an unfinished project that was in a pretty basic working state when I left it.

The idea is to make a tool that, when provided with a deck in JSON format, is able to scrape an online MTG store
looking for the cards in the deck, then the tool is supposed to make the process of building a deck from singles a lot easier by:

- computing the prices of the cards requested
- adding up the prices to know the total
- computing the list of remaining cards that could not be found in the provided store
- automatically generate a shopping cart that you can then load by copying a cookie (store dependent)


This tool was made specifically targeting a store in Argentina which does not have a way to automatically buy a whole deck from singles, but the idea was to make it so every store could have a different scrapy "spider" which is supposed to look for the cards in the specified store.

# Known issues / TODO
- bad design: the program in the current state is a proof of concept and not a finished product, extensibility is really hard and really bad